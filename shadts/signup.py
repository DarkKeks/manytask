import logging
import re

from flask import request, session, render_template, redirect, url_for

from . import app
from .database import CourseDatabase
from . import gitlab_repos
from . import helpers

logger = logging.getLogger(__name__)

def is_acceptable_group(group_name):
    if "ACCEPTABLE_GROUPS" in app.config:
        acceptable_groups = app.config["ACCEPTABLE_GROUPS"].split(',')
        return group_name in acceptable_groups
    return True


NAME_REGEXP=re.compile('^[a-zA-Z\-]+$')
def is_acceptable_name(name):
    return NAME_REGEXP.match(name) is not None


def fail_signup(reason):
    return render_template("signup.html", error_message=reason)

@app.route("/signup", methods=["GET", "POST"])
def signup():
    if request.method == "GET":
        return render_template("signup.html")

    db = CourseDatabase.from_config()

    if request.form["secret"] != app.config["REGISTRATION_SECRET"]:
        return fail_signup("Invalid secret code")

    group_name = request.form["group"] if "ACCEPTABLE_GROUPS" in app.config else "0"

    if not is_acceptable_name(request.form["firstname"]):
        return fail_signup("Invalid first name: only latin letters accepted")
    if not is_acceptable_name(request.form["lastname"]):
        return fail_signup("Invalid last name: only latin letters accepted")
    if not is_acceptable_group(group_name):
        return fail_signup("Invalid group name")

    if not gitlab_repos.is_gitlab_user_exists(request.form["username"]):
        return fail_signup("Invalid user name: Gitalab user does not exist")

    if db.has_user(request.form["username"]):
        return fail_signup("User with this username is already registered")

    db.save_user(
        request.form["username"],
        request.form["firstname"],
        request.form["lastname"],
        group_name)

    return redirect(url_for("login"))
