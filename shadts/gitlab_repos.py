import logging

logger = logging.getLogger(__name__)

import gitlab

from . import app

def make_gitlab_api():
    return gitlab.Gitlab(app.config["GITLAB_URL"], app.config["GITLAB_ROBOT_TOKEN"])

# -------------------------------------------------------------------

def format_project_name(user_info):
    projectname = '{}-z-{}-z-{}-z-{}'.format(
           user_info.groupname, user_info.firstname, user_info.lastname, user_info.username)
    return projectname

def get_project_name_for_user(user_info):
    return format_project_name(user_info)

def _format_project_name_url(user_info):
    def _replace_dots(username):
         return username

    return "{}-z-{}-z-{}-z-{}".format(
           user_info.groupname,
           user_info.firstname,
           user_info.lastname,
           _replace_dots(user_info.username))

def url_for_student_repo(user_info):
    return app.config["GITLAB_URL"] + "/" + \
           app.config["GITLAB_GROUP"] + "/" + \
           _format_project_name_url(user_info)

def url_for_student_submits(user_info):
    return app.config["GITLAB_URL"] + "/" + \
           app.config["GITLAB_GROUP"] + "/" + \
           _format_project_name_url(user_info) + "/-/jobs"

def is_gitlab_user_exists(username):
    gitlab_api = make_gitlab_api()
    user = gitlab_api.users.list(username=username)
    return len(user) == 1

# -------------------------------------------------------------------

def _find_existing_project(gitlab, course_group, project_name):
    projects = list(filter(lambda project: project.name == project_name,
                           course_group.projects.list(search=project_name)))

    if len(projects) > 0:
        return gitlab.projects.get(projects[0].id)

    return None

def check_master_branch(project):
    branches = project.branches.list()
    for branch in branches:
        if branch.name == "master":
            return branch
    return None

def has_runner(project, runner_id):
    runners = project.runners.list()
    for runner in runners:
        if runner.id == runner_id:
            return True
    return False

def has_user(project, user_id):
    for user in project.members.all(all=True):
        print(user)
        if user.id == user_id:
            return True
    return False

def maybe_create_project_for_user(user_info, user_id):
    username = user_info.username

    gitlab_api = make_gitlab_api()

    project_name = get_project_name_for_user(user_info)
    course_group = gitlab_api.groups.get(app.config["GITLAB_GROUP"])

    # Find or create repository

    project = _find_existing_project(gitlab_api, course_group, project_name)

    if not project:
        print("existing project not found, create new")
        project = gitlab_api.projects.create({
            "name": project_name,
            "namespace_id": course_group.id,
            "builds_enabled": True,
            "with_merge_requests_enabled": True,
            "shared_runners_enabled": False,
        })
    else:
        print("project alrady exists")
        logger.info("Project already exists: {}".format(project_name))

    # Prepare master

    print("prepare master")

    has_master = check_master_branch(project)

    readme_content = "# Solutions".format(username)

    if not has_master:
        print("create README.md")
        project.files.create({
            'file_path': 'README.md',
            'content': readme_content,
            'branch': 'master',
            'author_email': 'manytask@example.com',
            'author_name': 'manytask',
            'commit_message': 'initial commit',
        })

    print("protect master")
    project.branches.get('master').protect()

    # Enable specific runnner

    print("setup runner")

    ci_runner_id = int(app.config["GITLAB_CI_RUNNER_ID"]) if "GITLAB_CI_RUNNER_ID" in app.config else None

    if ci_runner_id:
        if not has_runner(project, ci_runner_id):
            print("runner not found")
            project.runners.create({'runner_id': ci_runner_id})
            logger.info("Set runner {} for project {}".format(ci_runner_id, project))

    logger.info("Git project created {}".format(project))

    # Add user to project


    if not has_user(project, user_id):
        print("register project member")
        member = project.members.create({
            "user_id": user_id,
            "project_id": project.id,
            "access_level": gitlab.DEVELOPER_ACCESS,
        })

    print ("completed")

    logger.info("Access to project granted to {}".format(user_id))
