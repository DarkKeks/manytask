import datetime
import pytz

MOSCOW_TIMEZONE = pytz.timezone('Europe/Moscow')

def now():
    return datetime.datetime.now(MOSCOW_TIMEZONE)

def parse_datetime(time):
    date = datetime.datetime.strptime(time, "%d-%m-%Y %H:%M")
    return MOSCOW_TIMEZONE.localize(date)

