import logging
import yaml
import datetime
import requests

from . import app
from . import helpers
from .scoring import ScoreCalculator

logger = logging.getLogger(__name__)


class Task:
    def __init__(self, group, config):
        self.group = group
        self.name = config["task"]
        self.score = config["score"]
        self.tags = config.get("tags", [])
        self.max_attempts = config.get("max_attempts")

    @property
    def homework(self):
        return self.name.split('/')[0]

    @property
    def short_name(self):
        return self.name.split('/')[1]

    def has_attempts_left(self, attempt):
        if self.max_attempts is not None:
            return attempt < self.max_attempts
        return True

    @property
    def current_score(self):
        return self.compute_current_score()

    def compute_current_score(self):
        now = helpers.now()
        return self.group.score_calculator.score(full_score=self.score, submit_time=now)


class TaskGroup:
    def __init__(self, config):
        self.name = config["group"]
        self.start = helpers.parse_datetime(config["start"])
        self.deadline = helpers.parse_datetime(config["deadline"])
        self.pretty_deadline = config["deadline"]
        self.tasks = [Task(self, c) for c in config.get("tasks", [])]
        self.score_calculator = ScoreCalculator(self.deadline)

    @property
    def is_open(self):
        return helpers.now() > self.start


class Deadlines:
    def __init__(self, config):
        self.groups = [TaskGroup(c) for c in config]

    def find_task(self, name):
        for g in self.groups:
            for task in g.tasks:
                if task.name == name:
                    return task
        raise KeyError("Task {} not found".format(name))

    @property
    def open_groups(self):
        return [g for g in self.groups if g.is_open]

    @property
    def task_names(self):
        names = []
        for g in self.groups:
            for task in g.tasks:
                names.append(task.name)
        return names


class CourseRepo:
    def __init__(self, gitlab, repo_name, master):
        self.gitlab = gitlab.rstrip('/')
        self.repo_name = repo_name.rstrip('/')
        self.repo_url = "{}/{}".format(self.gitlab, self.repo_name)
        self.repo = self.repo_url # be compatible
        self.master = master

    @staticmethod
    def from_config():
        return CourseRepo(app.config["GITLAB_URL"], app.config["COURSE_REPO"], app.config["COURSE_REPO_MASTER"])

    def deadlines_url(self, user_group):
        return "{}/raw/{}/deadlines/{}.yml".format(self.repo_url, self.master, user_group)

    def fetch_deadlines(self, user_group):
        raw_rsp = requests.get(self.deadlines_url(user_group))
        raw_rsp.raise_for_status()
        return Deadlines(yaml.safe_load(raw_rsp.content))

    @property
    def tasks(self):
        return "{}/blob/{}/tasks".format(self.repo_url, self.master)
