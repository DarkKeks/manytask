import os
import logging

from flask import session, Flask, url_for


app = Flask(__name__)
app.config.from_envvar('SHADTS_SETTINGS', silent=True)

gunicorn_logger = logging.getLogger("gunicorn.error")
app.logger.handlers = gunicorn_logger.handlers
app.logger.setLevel(gunicorn_logger.level)

# Fix resource caching
@app.context_processor
def override_url_for():
    return {"url_for": dated_url_for}


def dated_url_for(endpoint, **values):
    if endpoint == 'static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(app.root_path, endpoint, filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    return url_for(endpoint, **values)

@app.template_global()
def navbar():
    course_db = database.open()
    user_info = course_db.get_user_info(session["gitlab"]["username"])

    student_repo_url = gitlab_repos.url_for_student_repo(user_info)
    student_submits_url = gitlab_repos.url_for_student_submits(user_info)

    links = [
        ("/", "tasks", "Tasks"),
        (student_repo_url, "repo", "My Repo"),
        (student_submits_url, "submits", "submits"),
    ]

    links += [("/logout", "logout", "Logout")]

    return links

@app.template_global()
def need_show_group_names():
    return "ACCEPTABLE_GROUPS" in app.config

from . import signup
from . import login
from . import api
