import pytest
import uuid
import gitlab

from shadts import signup

from .test_app import test_app

@pytest.fixture
def test_gitlab(test_app):
    return gitlab.Gitlab(test_app.config["GITLAB_URL"], test_app.config["GITLAB_ADMIN_TOKEN"])


def test_signup(test_app, test_gitlab):
    with test_app.test_client() as client:
        username =  "test-" + uuid.uuid4().hex
        rsp = client.post("/signup", data={
            "username": username,
            "firstname": "Te",
            "lastname": "St",
            "email": username + "@examble.com",
            "password": uuid.uuid4().hex,
            "secret": test_app.config["REGISTRATION_SECRET"]
        })

        assert rsp.status_code == 302

        user = test_gitlab.users.list(username=username)[0]

        signup.maybe_create_project_for_user(username, user.id)
        signup.maybe_create_project_for_user(username, user.id)
