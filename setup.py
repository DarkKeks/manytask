from setuptools import setup

packages = {
    'shadts': 'shadts/'
}

package_data = {
    'shadts': ['templates/*.html', 'static/*.png', 'static/*.css']
}

setup(
    name='shadts',
    version='1.0',
    packages=packages,
    package_dir=packages,
    include_package_data=True,
    package_data=package_data,
    install_requires=[
        'flask',
    ])
